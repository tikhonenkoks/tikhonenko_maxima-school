﻿namespace ClassLibrary2;

public static class Replace
{
        public static void MyReplace(this int[] array)
        {
            for (int i = array.Length - 1; i >= 0; i--)
            {
                if (array[i] < 0)
                {
                    Console.Write($"{array[i] = 0}" + " ");
                }
                else
                {
                    Console.Write(array[i] + " ");
                }
            }
            Console.WriteLine();
        }
        
}