﻿namespace ClassLibrary1;

public partial class TVRemote
{
    public static bool Power { get; private set; }
}

public partial class TVRemote
{
    public static void PowerOn()
    {
        Power = true;
        Console.WriteLine("Включение");
    }

    public static void PowerOff()
    {
        Power = false;
        Console.WriteLine("Выключение");
    }
}