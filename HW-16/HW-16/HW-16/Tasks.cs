﻿namespace HW_16;

public static class Tasks
{
    // Задание 1
    public static void Task1(this List<int> list)
    {
        var nums1 = list
            .Where(i => i % 2 != 0)
            .Distinct();

        Console.WriteLine("Нечетные числа последовательности");
        foreach (var num in nums1)
        {
            Console.Write("\t" + num);
        }
    }

    // Задание 2
    public static void Task2(this List<int> list)
    {
        var nums2 = list
            .Where(i => i % 2 != 0)
            .OrderBy(order => order)
            .ToList()
            .Select(s => s.ToString());

        Console.WriteLine("Отсортированная последовательность");
        foreach (var num in nums2)
        {
            Console.Write("\t" + num);
        }
    }

    // Задание 3
    public static void Task3(this List<Clients> clients)
    {
        clients.Add(new Clients { Id = 0, Year = 2019, Month = 2, Hours = 627 });
        clients.Add(new Clients { Id = 1, Year = 2020, Month = 5, Hours = 104 });
        clients.Add(new Clients { Id = 2, Year = 2022, Month = 9, Hours = 18 });
        clients.Add(new Clients { Id = 3, Year = 2020, Month = 12, Hours = 22 });
        clients.Add(new Clients { Id = 4, Year = 2015, Month = 3, Hours = 418 });

        var result = clients
            .Where(last => last.Hours == clients.Select(s => s.Hours).Min())
            .Select(s => $"Продолжительность: {s.Hours} часа(ов) | Год: {s.Year} | Месяц: {s.Month}")
            .LastOrDefault();

        Console.WriteLine("\n" + result);
    }
}