﻿namespace HW_16;

public class Clients
{
    public int Id { get; set; }
    public int Year { get; set; }
    public int Month { get; set; }
    public int Hours { get; set; }
}