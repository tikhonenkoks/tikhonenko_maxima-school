﻿

using HW_16;

Random random = new Random();

List<int> list = new List<int>(10);

for (int i = 0; i < 10; i++)
{
    list.Add(random.Next(0, 50));
}

Console.WriteLine("Исходный массив");
foreach (var item in list)
{
    Console.Write("\t" + item);
}
Console.WriteLine("\n");

// Задание 1
list.Task1();
Console.WriteLine("\n");

// Задание 2
list.Task2();
Console.WriteLine("\n");

// Задание 3
List<Clients> clients = new List<Clients>();

clients.Task3();