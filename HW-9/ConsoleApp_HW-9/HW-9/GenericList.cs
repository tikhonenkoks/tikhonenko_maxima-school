﻿namespace HW_9;

public class GenericList<T>
{
    private T[] array;
    private int i;

    public GenericList(int item)
    {
        array = new T[item];
    }
    public void Add(T znach)
    {
        if (i == array.Length)
        {
            Console.WriteLine("Перезапустите программу!");
            return;
        }
        array[i] = znach;
        i++;
    }

    public void Print()
    {
        foreach (var z in array)
        {
            Console.WriteLine(z);
        }
    }
}