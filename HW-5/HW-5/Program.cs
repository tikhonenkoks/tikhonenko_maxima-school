﻿using System.Xml;

int[] mas = new int[10];
int i = 0;
int result = 0;
int SumChet = 0;
int SumNechet = 0;

while (i < 10)
                
    try
    {
        Console.Write("Введите число   ");
        mas[i] = Convert.ToInt32(Console.ReadLine());
        i++;
    }
    catch (Exception e)
    {
        Console.WriteLine("Некорректный ввод. Перезапустите программу");
    }  
            
for (int a = 0; a < mas.Length; a++)
{
    if (mas[a] % 2 == 0 && mas[a] > 0)
    {
        SumChet = SumChet + mas[a];
    }
    else if (mas[a] % 2 != 0 && mas[a] > 0)
    {
        SumNechet = SumNechet + mas[a];   
    }
}
Console.WriteLine();
Console.WriteLine("Сумма четных чисел " + SumChet);
Console.WriteLine("Сумма нечетных чисел " + SumNechet);
result = SumChet - SumNechet;
Console.WriteLine();
Console.WriteLine("Разница четных и нечетных чисел " + result);
Console.ReadKey();
