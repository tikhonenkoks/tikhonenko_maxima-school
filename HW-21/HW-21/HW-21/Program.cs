﻿Console.WriteLine($"С добрым утром! Затрак уже готовится! Поток: {Thread.CurrentThread.ManagedThreadId}");

await PourCoffee();
await Task.WhenAll(
    FryEgg(),
    FryBecon(),
    MakeToast()
);

await JamONBread();
await PourJuice();

Console.WriteLine($"Затрак готов. Поток: {Thread.CurrentThread.ManagedThreadId}");

async Task PourCoffee()
{
    Console.WriteLine($"Готовим кофе. Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1500);
}

async Task FryEgg()
{
    Console.WriteLine($"Варим яйца. Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1500);
} 

async Task FryBecon()
{
    Console.WriteLine($"Жарим бекон. Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1500);
}

async Task MakeToast()
{
    Console.WriteLine($"Готовим тосты. Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1500);
}

async Task JamONBread()
{
    Console.WriteLine($"Мажем джем на тосты. Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1500);
}

async Task PourJuice()
{
    Console.WriteLine($"Наливаем сок. Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1500);
}
