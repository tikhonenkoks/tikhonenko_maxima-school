﻿namespace StackLibrary1;

public class MyStack<T>
{
    private List<T> array; // стек

    public MyStack(int capacity)
    {
        array = new List<T>(capacity);
    }

    // Размерность стека
    public int Capacity
    {
        get { return array.Capacity; }
    }

    // добавление элемента
    public void Push(T item)
    {
        array.Add(item);
    }

    // Извлечение элемента
    public T Pop()
    {
        var item = array.LastOrDefault();
        array.RemoveAt(array.Count - 1);

        return item;
    }

    // вернуть элемент верхушки 
    public T Peek()
    {
        var item = array.LastOrDefault();

        return item;
    }

}