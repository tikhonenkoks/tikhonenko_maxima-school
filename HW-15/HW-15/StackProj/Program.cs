﻿using StackLibrary1;

MyStack<int> myStack = new MyStack<int>(5);

Random rand = new Random();
for (int i = 0; i < myStack.Capacity; i++)
{
    myStack.Push(rand.Next(-100, 100));
}

Console.WriteLine($"Первый элемент: {myStack.Peek()}");

Console.Write("Элементы стека: ");
for (int i = 0; i < myStack.Capacity; i++)
{
    Console.Write("  " + myStack.Pop());
}