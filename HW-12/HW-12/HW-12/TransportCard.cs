﻿namespace HW_12;

public delegate void MessangeEvent(string balance);

public class TransportCard
{
    private int price = 30;
    private int _balance;

    public event MessangeEvent OnRefill;
    public event MessangeEvent OnPayment;
    public event MessangeEvent OnNoMoney;

    public void Refill(int money)
    {
        _balance += money;
        if (money != 0)
        {
            OnRefill.Invoke(_balance.ToString());
        }
    }

    public void Payment()
    {
        if (_balance >= 30)
        {
            _balance -= price;
            OnPayment.Invoke(price.ToString());
        }
        else
        {
            OnNoMoney.Invoke(_balance.ToString());
        }
    }
}
