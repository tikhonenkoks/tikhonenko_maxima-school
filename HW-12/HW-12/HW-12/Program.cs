﻿using HW_12;

TransportCard transportCard = new TransportCard();

transportCard.OnPayment += CardPayment;
transportCard.OnRefill += CardRefill;
transportCard.OnNoMoney += (string balance) => Console.WriteLine($"Недостаточно средств для оплаты проезда. Ваш баланс: {balance}");

transportCard.Refill(120);
transportCard.Payment();
transportCard.Payment();
transportCard.Payment();
transportCard.Payment();
transportCard.Refill(40);
transportCard.Payment();
transportCard.Payment();

void CardRefill(string balance)
{
    Console.WriteLine($"Пополнение счета карты: {balance}");
}

void CardPayment(string price)
{
    Console.WriteLine($"Списание средств: {price}");
}