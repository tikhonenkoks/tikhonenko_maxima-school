﻿public class Man
{
    private string Name;
    private int Age;
    private int Height;
    private int Weight;
    private int ColorEye;
    public int Age_Property
    {
        get { return Age; }
        set { if (value < 120 && value > 0) Age = value;}
    }
    public int Height_Property
    {
        get { return Height; }
        set
        {
            if (value < 300 && value > 45)
            Height = value;
        }
    }
    public int Weight_Property
    {
        get { return Weight; }
        set
        {
            if (value < 400 && value > 3)
            Weight = value;
        }
    }

    public void Print()
    {
        Console.WriteLine("Введите имя");
        var man = new Man();
        man.Name = Console.ReadLine();
        man.Age_Property = GetRandomAge();
        man.Height_Property = GetRandomHeight();
        man.Weight_Property = GetRandomWeight();
        
        
        //if ( Man.Age_Property == 0 || Man.Height_Property == 0 || Man.Weight_Property == 0)
        //  Console.WriteLine("Перезапустите программу");

        Console.WriteLine($"Имя: {man.Name}");
        Console.WriteLine($"Возраст: {man.Age_Property}");
        Console.WriteLine($"Рост: {man.Height_Property}");
        Console.WriteLine($"Вес: {man.Weight_Property}");

        ColorEye color = global::ColorEye.Black;
            if (color == global::ColorEye.Black)
            Console.WriteLine($"Цвет глаз: {color}");

        /* Хотел через рандом определять цвет глаз экземпляра класса, но так и не смог разобраться
         switch (color)
        {
            case ColorEye.Green:
                Console.WriteLine("Зеленый цвет глаз");
                break;
            case 1:
                Console.WriteLine("Синий цвет глаз");
                break;
            case 2:
                Console.WriteLine("Серый цвет глаз");
                break;
            case 3:
                Console.WriteLine("Коричневый цвет глаз");
                break;
            case 4:
                Console.WriteLine("Черный цвет глаз");
                break;
        }
        */
        // ColorEye color = ColorEye.Black;
        //if (color == ColorEye.Black)
        //    Console.WriteLine($"Цвет глаз: {color}");

        Console.ReadKey();

        int GetRandomAge()
        {
            Random result = new Random();
            return result.Next(0, 120);
        }

        int GetRandomHeight()
        {
            Random result2 = new Random();
            return result2.Next(45, 300);
        }

        int GetRandomWeight()
        {
            Random result3 = new Random();
            return result3.Next(3, 400);
        }
    }
}
