﻿using ClassLibraryINT;

Console.Write("Введите количество элементов массива: ");

int n = int.Parse(Console.ReadLine());

int[] myArray = new int[n];

Random random = new Random();
for (int i = 0; i < myArray.Length; i++)
{
    myArray[i] = random.Next(-100, 100);
}

Console.WriteLine("Введенный массив: ");

for (int i = 0; i < myArray.Length; i++)
{
    Console.Write("  " + myArray[i]);
}
Console.WriteLine();


myArray.BubbleSort();
Console.WriteLine("Отсортированный массив: ");
for (int i = 0; i < myArray.Length; i++)
{
    Console.Write("  " + myArray[i]);
}
