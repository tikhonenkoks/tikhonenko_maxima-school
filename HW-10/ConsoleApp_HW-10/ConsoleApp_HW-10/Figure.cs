﻿namespace ConsoleApp_HW_10;

public abstract class Figure
{
    //private string name;

    public Figure(string name)
    {
        Name = name;
    }

    public string Name { get; set; }

    public double Square;
    public abstract void GetSquare();

    public virtual void Print()
    {
        Console.WriteLine("Название прямоугольника " + Name);
    }
}