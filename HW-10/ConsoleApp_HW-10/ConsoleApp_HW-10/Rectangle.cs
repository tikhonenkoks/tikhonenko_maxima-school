﻿namespace ConsoleApp_HW_10;

public class Rectangle : Figure
{
  public Rectangle(string name, double a, double b) : base(name)
  {
    Square = a * b;
  }

  public override void GetSquare()
  {
    Console.WriteLine("Площадь прямоугольника =" + Square);
  }
}