﻿// See https://aka.ms/new-console-template for more information

using ConsoleApp_HW_10;

Console.WriteLine("Введите название прямоугольника: ");
string NAME = Console.ReadLine();

Console.WriteLine("Введите длину прямоугольника: ");
double x = double.Parse(Console.ReadLine());

Console.WriteLine("Введите ширину прямоугольника: ");
double y = double.Parse(Console.ReadLine());


var rectangle = new Rectangle(NAME,x,y);
rectangle.GetSquare();
rectangle.Print();