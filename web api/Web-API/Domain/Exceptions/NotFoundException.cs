﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions
{
    /// <summary>
    /// Исключение : объект не найден
    /// </summary>
    public class NotFoundException : Exception
    {
        /// <summary>
        /// Исключение : объект не найден
        /// </summary>
        /// <param name="message">Сообщение об ошибке</param>
        public NotFoundException(string? message) : base(message)
        {

        }
    }
}
