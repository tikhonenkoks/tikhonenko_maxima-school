﻿using Domain.Tickets.Commands;
using FluentValidation;

/// <summary>
/// Валидатор CQRS модели удаления билета
/// </summary>
public class DeleteTicketCommandValidator : AbstractValidator<DeleteTicketCommand>
{
    /// <summary>
    /// Правила валидации CQRS модели удаления билета
    /// </summary>
    public DeleteTicketCommandValidator()
    {
        RuleFor(command => command.Id).NotNull().GreaterThanOrEqualTo(0);
    }
}