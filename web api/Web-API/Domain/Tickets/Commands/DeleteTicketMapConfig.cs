﻿using AutoMapper;
using DataAccessLayer.Models;
using Domain.Tickets.Commands;

namespace Domain.Tickets;

/// <summary>
/// Автомаппер CQRS модели удаления билета и модели в базе данных
/// </summary>
public class DeleteTicketMapConfig : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели удаления билета и модели в базе данных
    /// </summary>
    public DeleteTicketMapConfig()
    {
        CreateMap<DeleteTicketCommand, Ticket>()
            .ForMember(dto => dto.ID, expression => expression.MapFrom(ticket => ticket.Id));

        CreateMap<Ticket, DeleteTicketCommand>()
           .ForMember(dto => dto.Id, expression => expression.MapFrom(ticket => ticket.ID));
    }
}