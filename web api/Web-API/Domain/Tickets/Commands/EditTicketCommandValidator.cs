﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Tickets.Commands
{
    /// <summary>
    /// Валидатор CQRS модели редактирования билета
    /// </summary>
    public class EditTicketCommandValidator : AbstractValidator<TicketDto>
    {
        /// <summary>
        /// Правила валидации CQRS модели редактирования билета
        /// </summary>
        public EditTicketCommandValidator()
        {
            RuleFor(ticket => ticket.ID)
            .NotNull()
            .GreaterThan(0)
            .WithMessage("Поле должно быть больше 0");

            RuleFor(ticket => ticket.NameMovie)
            .NotEmpty()
            .MinimumLength(1)
            .MaximumLength(100)
            .WithMessage("Поле NameMovie должно быть от 1 до 100 символов");

            RuleFor(ticket => ticket.Genre)
            .NotEmpty()
            .MinimumLength(1)
            .MaximumLength(100)
            .WithMessage("Поле Genre должно быть от 1 до 100 символов");

            RuleFor(ticket => ticket.AgeRating)
            .NotEmpty()
            .MinimumLength(1)
            .MaximumLength(4)
            .WithMessage("Поле AgeRating должно быть от 1 до 4 символов");

            RuleFor(ticket => ticket.OrderNum)
            .NotEmpty()
            .GreaterThan(1)
            .LessThan(5000)
            .WithMessage("Поле OrderNum должно быть больше 1 и меньше 5000");

            RuleFor(ticket => ticket.Cost)
            .NotNull()
            .GreaterThan(0)
            .LessThan(1000)
            .WithMessage("Поле должно быть больше 0 и меньше 1000");


            RuleFor(ticket => ticket.Row)
            .NotNull()
            .GreaterThan(0)
            .LessThan(50)
            .WithMessage("Поле должно быть больше 0 и меньше 50");

            RuleFor(ticket => ticket.Place)
            .NotNull()
            .GreaterThan(0)
            .LessThan(50)
            .WithMessage("Поле должно быть больше 0 и меньше 50");
        }
    }
}

