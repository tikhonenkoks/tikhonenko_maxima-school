﻿using AutoMapper;
using DataAccessLayer.Models;
using Domain.Tickets.Commands;

namespace Domain.Tickets;

/// <summary>
/// Автомаппер CQRS модели создания билета и модели в базе данных
/// </summary>
public class CreateTicketMapConfig : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели создания билета и модели в базе данных
    /// </summary>
    public CreateTicketMapConfig()
    {
        CreateMap<CreateTicketCommand, Ticket>()
            .ForMember(dto => dto.ID, expression => expression.MapFrom(ticket => ticket.ID))
            .ForMember(dto => dto.NameMovie, expression => expression.MapFrom(ticket => ticket.NameMovie))
            .ForMember(dto => dto.Genre, expression => expression.MapFrom(ticket => ticket.Genre))
            .ForMember(dto => dto.AgeRating, expression => expression.MapFrom(ticket => ticket.AgeRating))
            .ForMember(dto => dto.OrderNum, expression => expression.MapFrom(ticket => ticket.OrderNum))
            .ForMember(dto => dto.SessionTime, expression => expression.MapFrom(ticket => ticket.SessionTime))
            .ForMember(dto => dto.Cost, expression => expression.MapFrom(ticket => ticket.Cost))
            .ForMember(dto => dto.HallNum, expression => expression.MapFrom(ticket => ticket.HallNum))
            .ForMember(dto => dto.Row, expression => expression.MapFrom(ticket => ticket.Row))
            .ForMember(dto => dto.Place, expression => expression.MapFrom(ticket => ticket.Place));

        CreateMap<Ticket, CreateTicketCommand>()
           .ForMember(dto => dto.ID, expression => expression.MapFrom(ticket => ticket.ID))
           .ForMember(dto => dto.NameMovie, expression => expression.MapFrom(ticket => ticket.NameMovie))
           .ForMember(dto => dto.Genre, expression => expression.MapFrom(ticket => ticket.Genre))
           .ForMember(dto => dto.AgeRating, expression => expression.MapFrom(ticket => ticket.AgeRating))
           .ForMember(dto => dto.OrderNum, expression => expression.MapFrom(ticket => ticket.OrderNum))
           .ForMember(dto => dto.SessionTime, expression => expression.MapFrom(ticket => ticket.SessionTime))
           .ForMember(dto => dto.Cost, expression => expression.MapFrom(ticket => ticket.Cost))
           .ForMember(dto => dto.HallNum, expression => expression.MapFrom(ticket => ticket.HallNum))
           .ForMember(dto => dto.Row, expression => expression.MapFrom(ticket => ticket.Row))
           .ForMember(dto => dto.Place, expression => expression.MapFrom(ticket => ticket.Place));
    }
}
