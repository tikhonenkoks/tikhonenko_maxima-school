﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Tickets.Commands
{
    /// <summary>
    /// CQRS обработчик создания билета
    /// </summary>
    public class CreateTicketCommandHandler : IRequestHandler<CreateTicketCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public CreateTicketCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды создания билета
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор нового билета</returns>
        public async Task<int> Handle(CreateTicketCommand request, CancellationToken cancellationToken)
        {
            var ticket = _mapper.Map<Ticket>(request);
            await _dataContext.Tickets.AddAsync(ticket, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return ticket.ID;
        }
    }
}
