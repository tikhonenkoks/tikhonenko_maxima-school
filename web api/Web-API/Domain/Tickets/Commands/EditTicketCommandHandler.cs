﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Tickets.Commands
{
    /// <summary>
    /// CQRS обработчик редактирования билета 
    /// </summary>
    public class EditTicketCommandHandler : IRequestHandler<EditTicketCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public EditTicketCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды редактирования билета
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Название фильма</returns>
        /// <exception cref="NotFoundException">Обработчик исключений : такого билета нет</exception>
        public async Task<int> Handle(EditTicketCommand request, CancellationToken cancellationToken)
        {
            var ticket = await _dataContext.Tickets
            .FirstOrDefaultAsync(ticket1 => ticket1.ID == request.ID, cancellationToken: cancellationToken);

            if (ticket == null)
                throw new NotFoundException($"Билет с ID: {request.ID} не найден");

            _mapper.Map(request, ticket);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return ticket.ID;
        }
    }
}

