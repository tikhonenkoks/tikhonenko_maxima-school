﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Domain.Exceptions;
using MediatR;

namespace Domain.Tickets.Commands
{
    /// <summary>
    /// CQRS обработчик удаления билета
    /// </summary>
    public class DeleteTicketCommandHandler : IRequestHandler<DeleteTicketCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public DeleteTicketCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды удаления билета
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор удаляемого билета</returns>
        /// <exception cref="NotFoundException">Обработчик исключений : такого билета нет</exception>
        public async Task<int> Handle(DeleteTicketCommand request, CancellationToken cancellationToken)
        {
            var ticket = _dataContext.Tickets
            .FirstOrDefault(ticket1 => ticket1.ID == request.Id);
            if (ticket != null)
            {
                _dataContext.Tickets.Remove(ticket);
                await _dataContext.SaveChangesAsync(cancellationToken);
                return ticket.ID;
            }
            throw new NotFoundException($"Билет с Id: {request.Id} не найден");
        }
    }
}
