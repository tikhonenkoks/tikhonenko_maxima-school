﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Tickets.Commands
{
    /// <summary>
    /// CQRS модель удаления билета 
    /// </summary>
    public class DeleteTicketCommand : IRequest< int>
    {
        /// <summary>
        /// Идентификатор билета
        /// </summary>
        public int Id { get; set; }
    }
}
