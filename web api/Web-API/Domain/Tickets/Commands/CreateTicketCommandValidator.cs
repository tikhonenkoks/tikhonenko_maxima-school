﻿using FluentValidation;

namespace Domain.Tickets.Commands;

/// <summary>
/// Валидатор CQRS модели создания билета
/// </summary>
public class CreateTicketCommandValidator : AbstractValidator<TicketDto>
{
    /// <summary>
    /// Правила валидации CQRS модели создания билета
    /// </summary>
    public CreateTicketCommandValidator()
    {
        RuleFor(ticket => ticket.ID)
        .NotNull()
        .GreaterThan(0)
        .WithMessage("Поле должно быть больше 0");

        RuleFor(ticket => ticket.NameMovie)
        .NotEmpty()
        .MinimumLength(1)
        .MaximumLength(100)
        .WithMessage("Поле NameMovie должно быть от 1 до 100 символов");

        RuleFor(ticket => ticket.OrderNum)
        .NotEmpty()
        .GreaterThan(1)
        .LessThan(5000)
        .WithMessage("Поле OrderNum должно быть больше 1 и меньше 5000");
    }
}

