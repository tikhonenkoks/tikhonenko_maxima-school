﻿using FluentValidation;

namespace Domain.Tickets.Queries;

/// <summary>
/// Валидатор CQRS модели получения билетjd
/// </summary>
public class GetTicketsQueryValidator : AbstractValidator<GetTicketsQuery>
{
    /// <summary>
    /// Правила валидации CQRS модели получения билетов
    /// </summary>
    public GetTicketsQueryValidator()
    {
        RuleFor(query => query.Skip).GreaterThanOrEqualTo(0);
        RuleFor(query => query.Take).GreaterThanOrEqualTo(0);
        RuleFor(query => query.Skip).Must((query, skip) => skip <= query.Take);
    }
}
