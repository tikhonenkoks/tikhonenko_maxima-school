﻿using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Tickets.Queries;

/// <summary>
/// CQRS обработчик получения билета
/// </summary>
public class GetTicketQueryHandler : IRequestHandler<GetTicketQuery, TicketDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Констуктор для добавления зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetTicketQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик запроса получения билета
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Билет</returns>
    /// <exception cref="NotFoundException">Обработчик исключений : такого билета нет</exception>
    public async Task<TicketDto> Handle(GetTicketQuery request, CancellationToken cancellationToken)
    {
        var ticket = await _dataContext.Tickets
            .AsNoTracking() //отключение отслеживания изменений
            .FirstOrDefaultAsync(ticket1 => ticket1.ID == request.Id);
        if (ticket == null)
            throw new NotFoundException($"Билет с id={request.Id} не найден");
        return _mapper.Map<TicketDto>(ticket);
    }
}
