﻿using FluentValidation;

namespace Domain.Tickets.Queries
{
    /// <summary>
    /// Валидатор CQRS модели получения билета
    /// </summary>
    public class GetTicketQueryValidator : AbstractValidator<GetTicketQuery>
    {
        /// <summary>
        /// Правила валидации CQRS модели получения билета
        /// </summary>
        public GetTicketQueryValidator()
        {
            RuleFor(query => query.Id).GreaterThanOrEqualTo(0);
        }
    }
}
