﻿using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Tickets.Queries;

/// <summary>
/// CQRS обработчик получения билетов
/// </summary>
public class GetTicketsQueryHandler : IRequestHandler<GetTicketsQuery, IEnumerable<TicketDto>>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Констуктор для добавления зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetTicketsQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик запроса получения билетов
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Коллекция билетов</returns>
    public async Task<IEnumerable<TicketDto>> Handle(GetTicketsQuery request, CancellationToken cancellationToken)
    {
        var tickets = await _dataContext.Tickets
            .Skip(request.Skip)
            .Take(request.Take)
            .AsNoTracking() //выключаем отслеживание для более быстрого выполнения запроса
            .ToListAsync(cancellationToken: cancellationToken);

        return _mapper.Map<IEnumerable<TicketDto>>(tickets);

    }
}
