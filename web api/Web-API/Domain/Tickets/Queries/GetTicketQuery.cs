﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Tickets.Queries;

/// <summary>
/// CQRS модель получения билета 
/// </summary>
public class GetTicketQuery : IRequest<TicketDto>
{
    /// <summary>
    /// Идентификатор билета
    /// </summary>
    public int Id { get; set; }
}
