﻿using MediatR;

namespace Domain.Tickets.Queries;

/// <summary>
/// CQRS модель получения билетов пагинацией
/// </summary>
public class GetTicketsQuery : IRequest<IEnumerable<TicketDto>>
{
    /// <summary>
    /// C какого ID хотим получить билеты
    /// </summary>
    public int Skip { get; set; }
    /// <summary>
    /// По какой ID хотим получить билеты
    /// </summary>
    public int Take { get; set; }
}