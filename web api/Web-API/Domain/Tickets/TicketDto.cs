﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Tickets;
/// <summary>
/// Билет 
/// </summary>
public partial class TicketDto
{
    /// <summary>
    /// Идентификатор билета
    /// </summary>
    public int ID { get; set; }
    /// <summary>
    /// Наименование билета
    /// </summary>
    public string? NameMovie { get; set; }
    /// <summary>
    /// Жанр
    /// </summary>
    public string? Genre { get; set; }
    /// <summary>
    /// Возрастной ценз
    /// </summary>
    public string? AgeRating { get; set; }
    /// <summary>
    /// Номер заказа
    /// </summary>
    public int OrderNum { get; set; }
    /// <summary>
    /// Время показа
    /// </summary>
    public DateTime SessionTime { get; set; }
    /// <summary>
    /// Цена
    /// </summary>
    public double Cost { get; set; }
    /// <summary>
    /// Номер зала
    /// </summary>
    public int HallNum { get; set; }
    /// <summary>
    /// Номер ряда
    /// </summary>
    public int Row { get; set; }
    /// <summary>
    /// Номер места 
    /// </summary>
    public int Place { get; set; }
}
