﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Domain.Sequrity;

/// <summary>
/// Создание токена JWT
/// </summary>
public class JWTProvider : IJWTProvider
{
    public readonly DomainOptions _options;

    public JWTProvider(IOptions<DomainOptions> options)
    {
        _options = options.Value;
    }

    public string GetToken(string login, string role, int id)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var keyval = _options.Secret;
        var key = Encoding.ASCII.GetBytes(keyval);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
            new Claim(ClaimsIdentity.DefaultNameClaimType, login),
            new Claim(ClaimsIdentity.DefaultRoleClaimType, role),
            new Claim("userId", id.ToString())
        }),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature),

        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var res = tokenHandler.WriteToken(token);
        return res;
    }
}
