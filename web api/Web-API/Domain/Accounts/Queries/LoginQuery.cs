﻿using MediatR;

namespace Domain.Accounts.Queries
{
    /// <summary>
    /// Авторизация логином и паролем
    /// </summary>
    public class LoginQuery : IRequest<string>
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
