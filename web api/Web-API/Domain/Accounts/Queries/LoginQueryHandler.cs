﻿using DataAccessLayer;
using Domain.Sequrity;
using MediatR;

namespace Domain.Accounts.Queries
{
    /// <summary>
    /// Обработка авторизации - логика авторизации
    /// </summary>
    public class LoginQueryHandler : IRequestHandler<LoginQuery, string>
    {
        private readonly DataContext _dataContext;
        private readonly IJWTProvider _jwtTokenProvider;

        /// <summary>
        /// Конструктор для добавления зависимости бд
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="jwtProvider"></param>
        public LoginQueryHandler(DataContext dataContext, IJWTProvider jwtProvider)
        {
            _dataContext = dataContext;
            _jwtTokenProvider = jwtProvider;
        }

        /// <summary>
        /// Метод обработки авторизации
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<string> Handle(LoginQuery request, CancellationToken cancellationToken)
        {
            var user = _dataContext.Users.First(user1 => user1.Login == request.Login && user1.Password == request.Password);
            return Task.FromResult(_jwtTokenProvider.GetToken(user.Login, user.Role.ToString(), user.ID));

        }
    }
}
