﻿using MediatR;

namespace Domain.Users.Queries;

/// <summary>
/// CQRS модель получения всех пользователей
/// </summary>
public class GetUsersQuery : IRequest<IEnumerable<UserDto>>
{
}