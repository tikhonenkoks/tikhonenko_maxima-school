﻿using Domain.Tickets.Queries;
using FluentValidation;

namespace Domain.Users.Queries
{
    /// <summary>
    /// Валидатор CQRS модели получения пользователя
    /// </summary>
    public class GetUserQueryValidator : AbstractValidator<GetTicketQuery>
    {
        /// <summary>
        /// Правила валидации CQRS модели получения пользователя
        /// </summary>
        public GetUserQueryValidator()
        {
            RuleFor(query => query.Id).GreaterThanOrEqualTo(0);
        }
    }
}
