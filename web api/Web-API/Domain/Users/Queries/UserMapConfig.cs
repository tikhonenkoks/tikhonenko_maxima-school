﻿using AutoMapper;
using DataAccessLayer.Models;
using Domain.Users;

namespace Web_API.Mapper
{
    /// <summary>
    /// Автомаппер пользователя и модели в базе данных
    /// </summary>
    public class UserMapConfig : Profile
    {
        /// <summary>
        /// Правила маппинга пользователя и модели в базе данных
        /// </summary>
        public UserMapConfig()
        {
            CreateMap<User, UserDto>()
                .ForMember(dto => dto.ID, expression => expression.MapFrom(user => user.ID))
                .ForMember(dto => dto.FirstName, expression => expression.MapFrom(user => user.FirstName))
                .ForMember(dto => dto.LastName, expression => expression.MapFrom(user => user.LastName))
                .ForMember(dto => dto.Age, expression => expression.MapFrom(user => user.Age))
                .ForMember(dto => dto.PhoneNumber, expression => expression.MapFrom(user => user.PhoneNumber))
                .ForMember(dto => dto.Role, expression => expression.MapFrom(user => user.Role))
                .ForMember(dto => dto.Login, expression => expression.MapFrom(user => user.Login))
                .ForMember(dto => dto.Password, expression => expression.MapFrom(user => user.Password));

            CreateMap<UserDto, User>()
                .ForMember(dto => dto.ID, expression => expression.MapFrom(user => user.ID))
                .ForMember(dto => dto.FirstName, expression => expression.MapFrom(user => user.FirstName))
                .ForMember(dto => dto.LastName, expression => expression.MapFrom(user => user.LastName))
                .ForMember(dto => dto.Age, expression => expression.MapFrom(user => user.Age))
                .ForMember(dto => dto.PhoneNumber, expression => expression.MapFrom(user => user.PhoneNumber))
                .ForMember(dto => dto.Role, expression => expression.MapFrom(user => user.Role))
                .ForMember(dto => dto.Login, expression => expression.MapFrom(user => user.Login))
                .ForMember(dto => dto.Password, expression => expression.MapFrom(user => user.Password));
        }
    }
}
