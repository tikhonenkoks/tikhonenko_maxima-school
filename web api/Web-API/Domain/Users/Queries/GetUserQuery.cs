﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Users.Queries;

/// <summary>
/// CQRS модель получения пользователя 
/// </summary>
public class GetUserQuery : IRequest<UserDto>
{
    /// <summary>
    /// Идентификатор пользователя
    /// </summary>
    public int Id { get; set; }
}
