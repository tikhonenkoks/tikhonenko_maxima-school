﻿using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Users.Queries;

/// <summary>
/// CQRS обработчик получения пользователя
/// </summary>
public class GetUserQueryHandler : IRequestHandler<GetUserQuery, UserDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Констуктор для добавления зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetUserQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик запроса получения пользователя
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Пользователь</returns>
    /// <exception cref="NotFoundException">Обработчик исключений : такого пользователя нет</exception>
    public async Task<UserDto> Handle(GetUserQuery request, CancellationToken cancellationToken)
    {
        var user = await _dataContext.Users
            .AsNoTracking() //отключение отслеживания изменений
            .FirstOrDefaultAsync(user1 => user1.ID == request.Id);
        if (user == null)
            throw new NotFoundException($"Пользователь с id={request.Id} не найден");
        return _mapper.Map<UserDto>(user);
    }
}
