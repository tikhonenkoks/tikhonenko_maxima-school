﻿namespace Domain.Users;

/// <summary>
/// Роли
/// </summary>
public enum Role
{
    Disable = 1,
    Guest = 2,
    User = 3,
    Admin = 4
}