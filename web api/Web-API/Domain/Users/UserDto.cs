﻿using DataAccessLayer.Models;

namespace Domain.Users;

/// <summary>
/// Класс пользователя 
/// </summary>
public class UserDto
{
    /// <summary>
    /// Идентификатор пользователя
    /// </summary>
    public int ID { get; set; }
    /// <summary>
    /// Логин
    /// </summary>
    public string Login { get; set; }
    /// <summary>
    /// Пароль
    /// </summary>
    public string Password { get; set; }
    /// <summary>
    /// Имя
    /// </summary>
    public string? FirstName { get; set; }
    /// <summary>
    /// Фамилия
    /// </summary>
    public string? LastName { get; set; }
    /// <summary>
    /// Возраст
    /// </summary>
    public int Age { get; set; }
    /// <summary>
    /// Номер телефона
    /// </summary>
    public string? PhoneNumber { get; set; }
    /// <summary>
    /// Уровень доступа, права пользователя
    /// </summary>
    public Role Role { get; set; }
}
