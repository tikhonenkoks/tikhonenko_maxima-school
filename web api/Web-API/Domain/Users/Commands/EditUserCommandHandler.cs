﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Users.Commands
{
    /// <summary>
    /// CQRS обработчик редактирования пользователя 
    /// </summary>
    public class EditUserCommandHandler : IRequestHandler<EditUserCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public EditUserCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды редактирования пользователя
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор пользователя</returns>
        /// <exception cref="NotFoundException">Обработчик исключений : такого пользователя нет</exception>
        public async Task<int> Handle(EditUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _dataContext.Tickets
            .FirstOrDefaultAsync(user1 => user1.ID == request.ID, cancellationToken: cancellationToken);

            if (user == null)
                throw new NotFoundException($"Пользователь с ID: {request.ID} не найден");

            _mapper.Map(request, user);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return user.ID;
        }
    }
}

