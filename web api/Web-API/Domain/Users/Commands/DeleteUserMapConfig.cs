﻿using AutoMapper;
using DataAccessLayer.Models;
using Domain.Users.Commands;

namespace Domain.Users;

/// <summary>
/// Автомаппер CQRS модели удаления пользователя и модели в базе данных
/// </summary>
public class DeleteUserMapConfig : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели удаления пользователя и модели в базе данных
    /// </summary>
    public DeleteUserMapConfig()
    {
        CreateMap<DeleteUserCommand, User>()
            .ForMember(dto => dto.ID, expression => expression.MapFrom(user => user.Id));

        CreateMap<User, DeleteUserCommand>()
           .ForMember(dto => dto.Id, expression => expression.MapFrom(user => user.ID));
    }
}