﻿using AutoMapper;
using DataAccessLayer.Models;
using Domain.Users.Commands;

namespace Domain.Users;

/// <summary>
/// Автомаппер CQRS модели создания пользователя и модели в базе данных
/// </summary>
public class CreateUserMapConfig : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели создания пользователя и модели в базе данных
    /// </summary>
    public CreateUserMapConfig()
    {
        CreateMap<CreateUserCommand, User>()
            .ForMember(dto => dto.ID, expression => expression.MapFrom(user => user.ID))
            .ForMember(dto => dto.Login, expression => expression.MapFrom(user => user.Login))
            .ForMember(dto => dto.FirstName, expression => expression.MapFrom(user => user.FirstName))
            .ForMember(dto => dto.LastName, expression => expression.MapFrom(user => user.LastName))
            .ForMember(dto => dto.Age, expression => expression.MapFrom(user => user.Age))
            .ForMember(dto => dto.PhoneNumber, expression => expression.MapFrom(user => user.PhoneNumber))
            .ForMember(dto => dto.Role, expression => expression.MapFrom(user => user.Role))
            .ForMember(dto => dto.Password, expression => expression.MapFrom(user => user.Password));

        CreateMap<User, CreateUserCommand>()
            .ForMember(dto => dto.ID, expression => expression.MapFrom(user => user.ID))
            .ForMember(dto => dto.Login, expression => expression.MapFrom(user => user.Login))
            .ForMember(dto => dto.FirstName, expression => expression.MapFrom(user => user.FirstName))
            .ForMember(dto => dto.LastName, expression => expression.MapFrom(user => user.LastName))
            .ForMember(dto => dto.Age, expression => expression.MapFrom(user => user.Age))
            .ForMember(dto => dto.PhoneNumber, expression => expression.MapFrom(user => user.PhoneNumber))
            .ForMember(dto => dto.Role, expression => expression.MapFrom(user => user.Role))
            .ForMember(dto => dto.Password, expression => expression.MapFrom(user => user.Password));
    }
}
