﻿using FluentValidation;

namespace Domain.Users.Commands
{
    /// <summary>
    /// Валидатор CQRS модели редактирования пользователя
    /// </summary>
    public class EditUserCommandValidator : AbstractValidator<UserDto>
    {
        /// <summary>
        /// Правила валидации CQRS модели редактирования пользователя
        /// </summary>
        public EditUserCommandValidator()
        {
            RuleFor(user => user.ID)
            .NotEmpty()
            .GreaterThanOrEqualTo(0);

            RuleFor(user => user.Login)
                .NotNull()
                .NotEmpty()
                .MinimumLength(3)
                .MaximumLength(20)
                .WithMessage("Поле Login должно содержать от 3 до 20 символов");

            RuleFor(user => user.Password)
                .NotNull()
                .NotEmpty()
                .MinimumLength(8)
                .MaximumLength(20)
                .WithMessage("Поле Login должно содержать от 8 до 20 символов");

            RuleFor(user => user.FirstName)
                .NotNull()
                .NotEmpty()
                .MinimumLength(1)
                .MaximumLength(100)
                .WithMessage("Поле FirstName должно быть от 1 до 100");

            RuleFor(user => user.LastName)
                .NotNull()
                .NotEmpty()
                .MinimumLength(1)
                .MaximumLength(100)
                .WithMessage("Поле LastName должно быть от 1 до 100");

            RuleFor(user => user.Age)
                .NotNull()
                .NotEmpty()
                .GreaterThan(0)
                .LessThan(120);

            RuleFor(user => user.PhoneNumber)
                .MinimumLength(7)
                .MaximumLength(11);

            RuleFor(user => user.Role)
                .NotNull()
                .IsInEnum();
        }
    }
}

