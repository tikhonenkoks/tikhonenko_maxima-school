﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Users.Commands
{
    /// <summary>
    /// CQRS модель удаления пользователя 
    /// </summary>
    public class DeleteUserCommand : IRequest<int>
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int Id { get; set; }
    }
}
