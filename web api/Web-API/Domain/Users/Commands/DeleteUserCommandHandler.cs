﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Domain.Exceptions;
using MediatR;

namespace Domain.Users.Commands
{
    /// <summary>
    /// CQRS обработчик удаления пользователя
    /// </summary>
    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public DeleteUserCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды удаления пользователя
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор удаляемого билета</returns>
        /// <exception cref="NotFoundException">Обработчик исключений : такого пользователя нет</exception>
        public async Task<int> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var user = _dataContext.Tickets
            .FirstOrDefault(user1 => user1.ID == request.Id);
            if (user != null)
            {
                _dataContext.Tickets.Remove(user);
                await _dataContext.SaveChangesAsync(cancellationToken);
                return user.ID;
            }
            throw new NotFoundException($"Билет с Id: {request.Id} не найден");
        }
    }
}
