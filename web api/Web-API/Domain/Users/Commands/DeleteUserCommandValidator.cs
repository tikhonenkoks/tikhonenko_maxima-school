﻿using Domain.Users.Commands;
using FluentValidation;

/// <summary>
/// Валидатор CQRS модели удаления билета
/// </summary>
public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
{
    /// <summary>
    /// Правила валидации CQRS модели удаления пользователя
    /// </summary>
    public DeleteUserCommandValidator()
    {
        RuleFor(command => command.Id)
            .NotNull()
            .GreaterThanOrEqualTo(0);
    }
}