﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    /// <summary>
    /// Свойство Secret
    /// </summary>
    public class DomainOptions
    {
        public string Secret { get; set; }
    }
}
