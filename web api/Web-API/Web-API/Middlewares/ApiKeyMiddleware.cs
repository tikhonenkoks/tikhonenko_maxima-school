﻿/*using Web_API.Extensions;
using Web_API.Services;

namespace Web_API.Middlewares;

/// <summary>
/// Класс для проверки apikey - header для доступа к запросу(упрощенная авторизация)
/// </summary>
public class ApiKeyMiddleware : IMiddleware
{
    private readonly IApiKeyProvider _apiKeyProvider;

    /// <summary>
    /// Конструктор добавления зависимости
    /// </summary>
    /// <param name="apiKeyProvider"></param>
    public ApiKeyMiddleware(IApiKeyProvider apiKeyProvider)
    {
        _apiKeyProvider = apiKeyProvider;
    }


    // private RequestDelegate _next;
    //
    // public ApiKeyMiddleware(RequestDelegate next)
    // {
    //     _next = next;
    // }

    // public async Task InvokeAsync(HttpContext context, IApiKeyProvider apiKeyProvider)
    // {
    //     if (context.Request.Query["apikey"] == apiKeyProvider.GetKey()) 
    //     {
    //         await _next.Invoke(context);
    //     }
    //     else
    //     {
    //         context.Response.StatusCode = StatusCodes.Status403Forbidden;
    //     }

    /// <summary>
    /// Выполняется заданный делегат асинхронно в потоке
    /// </summary>
    /// <param name="context"></param>
    /// <param name="next"></param>
    /// <returns></returns>
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        if (context.Request.Query["apikey"] == _apiKeyProvider.GetKey())
        {
            await next.Invoke(context);
        }
        else
        {
            context.Response.StatusCode = StatusCodes.Status403Forbidden;
        }
    }
}*/