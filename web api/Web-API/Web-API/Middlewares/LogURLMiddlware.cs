﻿namespace Web_API;

/// <summary>
/// Класс внедрения логирования запросов
/// </summary>
public class LogURLMiddlware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<LogURLMiddlware> _logger;

    /// <summary>
    /// Конструктор для добавления зависимостей делегатов и логгера
    /// </summary>
    /// <param name="next"></param>
    /// <param name="loggerFactory"></param>
    /// <exception cref="ArgumentNullException"></exception>
    public LogURLMiddlware(RequestDelegate next, ILoggerFactory loggerFactory)
    {
        _next = next;
        _logger = loggerFactory?.CreateLogger<LogURLMiddlware>() ??
                  throw new ArgumentNullException(nameof(loggerFactory));
    }

    /// <summary>
    /// Выполняется заданный делегат асинхронно в потоке
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    public async Task InvokeAsync(HttpContext context)
    {
        _logger.LogInformation($"Request URL: {0}", context);
        await this._next(context);
    }
}