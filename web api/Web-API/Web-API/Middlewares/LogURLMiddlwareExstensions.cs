﻿namespace Web_API;

/// <summary>
/// Класс внедрения логирования исключений
/// </summary>
public static class LogURLMiddlwareExstensions
{
    /// <summary>
    /// Интерфейс для логирования исключений
    /// </summary>
    /// <param name="app"></param>
    /// <returns></returns>
    public static IApplicationBuilder UseLogUrl(this IApplicationBuilder app)
    {
        return app.UseMiddleware<LogURLMiddlware>();
    }
}