﻿using System;
using System.Threading.Tasks;
using Domain.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;


namespace Web_API.Middlewares;

/// <summary>
/// Обработчик исключений 
/// </summary>
public class ExceptionsHandlerCustomMiddleware
{
    private RequestDelegate _next;
    private ILogger<ExceptionsHandlerCustomMiddleware> _logger;

    /// <summary>
    /// Конструктор обработчика
    /// </summary>
    /// <param name="next"></param>
    /// <param name="logger"></param>
    public ExceptionsHandlerCustomMiddleware(RequestDelegate next, ILogger<ExceptionsHandlerCustomMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    /// <summary>
    /// Выполняется заданный делегат асинхронно в потоке
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _next.Invoke(context);
        }
        catch (NotFoundException NotFoundException)
        {
            _logger.LogError(NotFoundException.Message);
            context.Response.StatusCode = StatusCodes.Status404NotFound;
            await context.Response.WriteAsync(NotFoundException.Message);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception.Message);
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            await context.Response.WriteAsync(exception.Message);
        }
    }
}

