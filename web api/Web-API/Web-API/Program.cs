using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;
using DataAccessLayer;
using Web_API.Mapper;
using Web_API.Extensions;
using Web_API;
using Domain.Tickets.Commands;
using MediatR;
using Domain.Tickets;
using Domain.Tickets.Queries;
using Domain.Sequrity;
using Domain.Users;
using Domain.Users.Commands;
using Domain.Users.Queries;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.OpenApi.Models;
using Web_API.Requirements;
using Microsoft.AspNetCore.Authorization;
using Domain;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog ((context, configuration) =>
{
    configuration.MinimumLevel.Debug();
    configuration.WriteTo.Console();
    configuration.WriteTo.File("log.txt");
});

var applicationOptions = builder.Configuration.Get<ApplicationOptions>();

builder.Services.Configure<DomainOptions>(builder.Configuration); // ���������� ������������ � ���������
builder.Services.Configure<ApplicationOptions>(builder.Configuration); // ���������� ������������ � ���������

builder.Services.AddDbContext<DataContext>(optionsBuilder => optionsBuilder.UseNpgsql(applicationOptions.DbConnection));
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    {
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Web-API.xml"));
    }
    { 
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Domain.xml"));
    }
    {
        options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
        {
            In = ParameterLocation.Header,
            Description = "Please insert JWT with Bearer into field",
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey,
            BearerFormat = "JWT"
        });
    }
    options.AddSecurityRequirement(new OpenApiSecurityRequirement()
         {
             {
                 new OpenApiSecurityScheme
                 {
                     Reference = new OpenApiReference {Type = ReferenceType.SecurityScheme, Id = "Bearer"}
                 },
                 new string[] { }
             }
         });

    options.CustomSchemaIds(y => y.FullName);
});

builder.Services.AddTransient<IJWTProvider, JWTProvider>();// ���������� ���������� ��� ������ � �������� JWT

builder.Services.AddFluentValidationAutoValidation(); // ��������� �������������� ��������� Fluent Validation

builder.Services.AddValidatorsFromAssemblyContaining<CreateUserCommandValidator>(); // ���������� ���������� CreateUserCommandValidator
builder.Services.AddValidatorsFromAssemblyContaining <EditUserCommandValidator> (); // ���������� ���������� EditUserCommandValidator
builder.Services.AddValidatorsFromAssemblyContaining<GetUserQueryValidator>(); // ���������� ���������� GetUserQueryValidator

builder.Services.AddValidatorsFromAssemblyContaining<CreateTicketCommandValidator>(); // ���������� ���������� TicketDtoValidator
builder.Services.AddValidatorsFromAssemblyContaining<DeleteTicketCommandValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<EditTicketCommandValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<GetTicketQueryValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<GetTicketsQueryValidator>();

builder.Services.AddAutoMapper(
    typeof(UserMapConfig), typeof(TicketMapConfig), 
    typeof(CreateTicketMapConfig), typeof(CreateUserMapConfig), 
    typeof(EditTicketMapConfig), typeof(EditUserMapConfig),
    typeof(DeleteTicketMapConfig)); // ���������� �����������

builder.Services.AddMediatR(
    typeof(CreateTicketCommand), typeof(CreateUserCommand),
    typeof(GetTicketQuery), typeof(GetUserQuery),
    typeof(EditTicketCommand), typeof(EditUserCommand),
    typeof(GetTicketsQuery), typeof(GetUsersQuery),
    typeof(DeleteTicketCommand));

builder.Services.AddTransient<IAuthorizationHandler, MinimalAgeRequirementHandler>();

builder.Services.AddAuthentication(options =>
     {
         options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
         options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
         options.RequireAuthenticatedSignIn = false;
     })
     .AddJwtBearer(options =>
     {
         options.RequireHttpsMetadata = false;
         options.SaveToken = true;
         options.TokenValidationParameters = new TokenValidationParameters
         {
             ValidateIssuerSigningKey = true,
             IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(applicationOptions.Secret)),
             ValidateIssuer = false,
             ValidateAudience = false
         };
     });

builder.Services.AddAuthorization(options => 
    options.AddPolicy(Policies.OnlyAdultPolicy, policyBuilder => policyBuilder.AddRequirements(new MinimalAgeRequirement(18))));

var app = builder.Build();

app.UseExceptionsHandlerCustomMiddleware(); //���������� ��������� ��������� ����������
app.UseLogUrl(); //���������� ������������

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseReDoc(options => options.RoutePrefix = "redoc");
}

app.UseHttpsRedirection();
app.UseAuthentication(); // ��������� ���������� �� ��������������
app.UseAuthorization(); // ��������� ���������� �� �����������
app.MapControllers();
app.Run();
