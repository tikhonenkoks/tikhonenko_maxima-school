﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Domain.Users;
using Microsoft.EntityFrameworkCore;

namespace Web_API.Services
{
    /// <summary>
    /// Реализация методов для работы с User
    /// </summary>
    public class UserService : IUserService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Конструктор для добавления зависимостей datacontext и автомаппера
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public UserService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// Получение User по идентификатору
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDto? Get(int userId)
        {
            var user = _dataContext.Users
            .AsNoTracking() //отключение отслеживания изменений
            .FirstOrDefault(user1 => user1.ID == userId);
            if (user != null)
            {
                return _mapper.Map<UserDto>(user);
            };
            return null;
        }

        /// <summary>
        /// Получение всех User
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserDto> GetAll()
        {
            var userDto = _dataContext.Users
            .AsNoTracking() //отключение отслеживания изменений
            .Select(user => _mapper.Map<UserDto>(user))
           .ToList();
            return userDto;
        }

        /// <summary>
        /// Создание User
        /// </summary>
        /// <param name="userDto"></param>
        public void Create(UserDto userDto)
        {
            var user = _mapper.Map<User>(userDto);
            _dataContext.Users.Add(user);
            _dataContext.SaveChanges();
        }

        /// <summary>
        /// Удаление User по идентификатору
        /// </summary>
        /// <param name="userId"></param>
        public void Remove(int userId)
        {
            var userDto = _dataContext.Users
                .FirstOrDefault(user => user.ID == userId);
            if (userDto != null)
                _dataContext.Users.Remove(userDto);
            _dataContext.SaveChanges();
        }

        /// <summary>
        /// Редактирование User
        /// </summary>
        /// <param name="userDto"></param>
        public void Update(UserDto userDto)
        {
            var user = _dataContext.Users.FirstOrDefault(user1 => user1.ID == userDto.ID);
            if (user == null) return;
            user.Age = userDto.Age;
            user.FirstName = userDto.FirstName;
            user.LastName = userDto.LastName;
            _dataContext.SaveChanges();
        }
    }
}
