﻿using Domain.Users;

namespace Web_API.Services
{
    /// <summary>
    /// Набор методов для работы с User
    /// </summary>
    public interface IUserService 
    {
        /// <summary>
        /// Получение User по идентификатору
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserDto Get(int userId);
        /// <summary>
        /// Получение всех User
        /// </summary>
        /// <returns></returns>
        IEnumerable<UserDto> GetAll();
        /// <summary>
        ///  Создание User
        /// </summary>
        /// <param name="userDto"></param>
        void Create(UserDto userDto);
        /// <summary>
        /// Удаление User по идентификатору
        /// </summary>
        /// <param name="userId"></param>
        void Remove(int userId);
        /// <summary>
        /// Редактирование User
        /// </summary>
        /// <param name="userDto"></param>
        void Update(UserDto userDto);
    }
}
