﻿namespace Web_API.Services;

/// <summary>
/// Интерфейс для apikey авторизации
/// </summary>
public interface IApiKeyProvider
{
    /// <summary>
    /// Метод получения ключа
    /// </summary>
    /// <returns></returns>
    string GetKey();
}