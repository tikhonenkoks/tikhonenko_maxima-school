﻿using Domain.Tickets;

namespace Web_API.Services
{
    /// <summary>
    /// Набор методов для работы с Ticket
    /// </summary>
    public interface ITicketService
    {
        /// <summary>
        /// Получение Ticket по идентификатору
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        TicketDto Get(int ticketId);
        /// <summary>
        /// Получение всех Ticket
        /// </summary>
        /// <returns></returns>
        IEnumerable<TicketDto> GetAll();
        /// <summary>
        /// Создание Ticket 
        /// </summary>
        /// <param name="ticketDto"></param>
        void Create(TicketDto ticketDto);
        /// <summary>
        /// Удаление Ticket по идентификатору
        /// </summary>
        /// <param name="ticketId"></param>
        void Remove(int ticketId);
        /// <summary>
        /// Редактирование Ticket
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ticketDto"></param>
        void Update(int id, TicketDto ticketDto);
    }
}

