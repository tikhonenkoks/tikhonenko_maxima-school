﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Domain.Tickets;

namespace Web_API.Services
{
    /// <summary>
    /// Реализация методов для работы с Ticket
    /// </summary>
    public class TicketService : ITicketService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Конструктор для добавления зависимостей datacontext и автомаппера
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public TicketService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// Создание Ticket 
        /// </summary>
        /// <param name="ticketDto"></param>
        public void Create(TicketDto ticketDto)
        {
            var ticket = _mapper.Map<Ticket>(ticketDto);
            _dataContext.Tickets.Add(ticket);
            _dataContext.SaveChanges();
        }

        /// <summary>
        /// Получение Ticket по идентификатору
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        public TicketDto? Get(int ticketId)
        {
            var ticket = _dataContext.Tickets
            .AsNoTracking() //отключение отслеживания изменений
            .FirstOrDefault(ticket1 => ticket1.ID == ticketId);
            if (ticket != null)
            {
                return _mapper.Map<TicketDto>(ticket);
            };
            return null;
        }

        /// <summary>
        /// Получение всех Ticket
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TicketDto> GetAll()
        {
            var ticketDto = _dataContext.Tickets
            .AsNoTracking() //отключение отслеживания изменений
            .Select(ticketDto => _mapper.Map<TicketDto>(ticketDto))
            .ToList();
            return ticketDto;
        }

        /// <summary>
        /// Удаление Ticket по идентификатору
        /// </summary>
        /// <param name="ticketId"></param>
        public void Remove(int ticketId)
        {
            var x = _dataContext.Tickets
                .FirstOrDefault(ticket => ticket.ID == ticketId);
            if (x != null)
                _dataContext.Remove(x);
            _dataContext.SaveChanges();
        }

        /// <summary>
        /// Редактирование Ticket
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="ticketDto"></param>
        public void Update(int ticketId, TicketDto ticketDto)
        {
            var x = _dataContext.Tickets
                .FirstOrDefault(ticket1 => ticket1.ID == ticketId);
            if (x != null)
                x.NameMovie = ticketDto.NameMovie;
                x.AgeRating = ticketDto.AgeRating;
                x.HallNum = ticketDto.HallNum;
                x.OrderNum = ticketDto.OrderNum;
                x.Cost = ticketDto.Cost;
                x.Genre = ticketDto.Genre;
                x.Row = ticketDto.Row;
                x.Place = ticketDto.Place;

            _dataContext.SaveChanges();
        }
    }
}

