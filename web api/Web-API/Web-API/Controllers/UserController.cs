﻿using Microsoft.AspNetCore.Mvc;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;
using Web_API.Services;
using Domain.Users;
using MediatR;
using Domain.Users.Queries;
using Domain.Users.Commands;
using Microsoft.AspNetCore.Authorization;

namespace Web_API.Controllers;

[Authorize]
/// <summary>
/// Контроллер для UserDto
/// </summary>
[ApiController]
[Route("api/[controller]")]

public class UserController : ControllerBase
{
    //private readonly IUserService _userService; // создали зависимость от интерфейса IUserService
    private readonly IMediator _mediator;

    /// <summary>
    /// Конструктор для контроллера
    /// </summary>
    /// <param name="userService">Экземпляр userService</param>
    public UserController(IMediator mediator) // в конструкторе прописана зависимость
    {
        _mediator = mediator;
    }

    [Authorize(Roles = "Admin, User")]
    /// <summary>
    /// Получение пользователя по id
    /// </summary>
    /// <param name="id">Идентификатор пользователя</param>
    /// <returns>Пользователь</returns>
    [HttpGet("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    //[ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Get(int id)
    {
        var getUserQuery = new GetUserQuery() { Id = id };
        var userDto = await _mediator.Send(getUserQuery);
        return Ok(userDto);
    }

    [Authorize(Policy = Policies.OnlyAdultPolicy)]
    /// <summary>
    /// Получение всех пользователей
    /// </summary>
    /// <returns>Результирующий набор пользователей</returns>
    [HttpGet]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(IEnumerable<UserDto>), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult<IEnumerable<UserDto>>> GetAll([FromQuery] GetUsersQuery getUsersQuery)
    {
        var usersDto = await _mediator.Send(getUsersQuery);
        return Ok(usersDto);
    }

    /// <summary>
    /// Создание пользователя
    /// </summary>
    /// <param name="createUserCommand">Экземпляр CreateUserCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Create([FromBody] CreateUserCommand createUserCommand)
    {
        await _mediator.Send(createUserCommand);
        return Ok();
    }

    /// <summary>
    /// Изменение пользователя по идентификаатору
    /// </summary>
    /// <param name="id">Идентификаатор</param>
    /// <param name="editUserCommand">Экземпляр EditUserCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPut]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Edit([FromBody] EditUserCommand editUserCommand)
    {
        var user = await _mediator.Send(editUserCommand);
        return Ok(user);
    }
}

