﻿namespace Web_API.Controllers;

/// <summary>
/// Класс рукотворной кастомной проверки на ошибки
/// </summary>
/// <typeparam name="T"></typeparam>
public class ApiResult<T> : ApiResult
{
    /// <summary>
    /// Поля для кастомной проверки на ошибки
    /// </summary>
    public T? Data { get; set; }
}

/// <summary>
/// Класс рукотворной кастомной проверки на ошибки
/// </summary>
public class ApiResult
{
    /// <summary>
    ///  Поле для кастомной проверки на ошибки
    /// </summary>
    public bool Success { get; set; }
    /// <summary>
    /// Поле для кастомной проверки на ошибки
    /// </summary>
    public int StatusCode { get; set; }
    /// <summary>
    /// Поле для кастомной проверки на ошибки
    /// </summary>
    public string? Errors { get; set; }
}