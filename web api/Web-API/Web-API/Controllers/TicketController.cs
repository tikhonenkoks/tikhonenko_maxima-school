﻿using Domain.Tickets;
using Domain.Tickets.Commands;
using Domain.Tickets.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web_API.Services;

namespace Web_API.Controllers;

[Authorize]
/// <summary>
/// Контроллер для TicketDto
/// </summary>
[ApiController]
[Route("api/[controller]/[action]")]
public class TicketController : ControllerBase
{
    private readonly IMediator _mediator;

    /// <summary>
    /// Конструктор для контроллера
    /// </summary>
    /// <param name="ticketService">Экземпляр ticketService</param>
    /// <param name="mediator">медиатор для работы с CQRS</param>
    public TicketController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Получение билета по id 
    /// </summary>
    /// <param name="id">Идентификатор билета</param>
    /// <returns>Билет</returns>
    [Authorize(Roles = "Admin, User, Guest")]
    [HttpGet("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(TicketDto), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Get(int id)
    {
        var getTicketQuery = new GetTicketQuery() { Id = id };
        var ticketDto = await _mediator.Send(getTicketQuery);
        return Ok(ticketDto);
    }

    /// <summary>
    /// Получение билетов
    /// </summary>
    /// <param name="getTicketsQuery"> пагинация</param>
    /// <returns> Результирующий набор билетов </returns>
    [Authorize(Roles = "Admin, User")]
    [HttpGet]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(IEnumerable<TicketDto>), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult<IEnumerable<TicketDto>>> Get([FromQuery] GetTicketsQuery getTicketsQuery)
    {
        var ticketsDto = await _mediator.Send(getTicketsQuery);
        return Ok(ticketsDto);
    }

    /// <summary>
    /// Создание билетa
    /// </summary>
    /// <param name="createTicketCommand">Экземпляр команды CQRS createTicketCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Create([FromBody]CreateTicketCommand createTicketCommand)
    {
        await _mediator.Send(createTicketCommand);
        return Ok();
    }

    /// <summary>
    /// Изменение билета
    /// </summary>
    /// <param name="editTicketCommand">Экземпляр команды CQRS editTicketCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPut("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации
    public async Task<ActionResult> Edit([FromBody] EditTicketCommand editTicketCommand)
    {
        var ticket = await _mediator.Send(editTicketCommand);
        return Ok(ticket);
    }

    /// <summary>
    /// Удаление билета по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации
    public async Task<ActionResult> Delete([FromRoute] int id)
    {
        var ticket = new DeleteTicketCommand() { Id = id };
        await _mediator.Send(ticket);
        return Ok(ticket);
    }
}  