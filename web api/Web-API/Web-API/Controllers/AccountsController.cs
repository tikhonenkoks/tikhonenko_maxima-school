﻿using Domain.Accounts.Queries;
using Domain.Sequrity;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Web_API.Controllers
{
    [Authorize]
    /// <summary>
    /// Контроллер для авторизации
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class AccountsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AccountsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [AllowAnonymous]
        [HttpGet("/login")]
        public async Task<ActionResult<string>> Login([FromQuery] LoginQuery loginQuery)
        {
            try
            {
                var jwtToken = await _mediator.Send(loginQuery);
                return Ok(jwtToken);
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("/me")]
        public ActionResult<string> GetMyName()
        {
            return HttpContext.User.Identity.Name;
        }

    }

}
