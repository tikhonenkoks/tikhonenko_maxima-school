﻿/*using FluentValidation;

namespace Web_API.Models;
/// <summary>
/// Валидатор пользователя
/// </summary>
public class UserDtoValidator : AbstractValidator<UserDto>
{
    /// <summary>
    /// Правила валидации пользователя
    /// </summary>
    public UserDtoValidator()
    {
        RuleFor(user => user.ID)
            .NotEmpty().
            GreaterThanOrEqualTo(0);
        
        RuleFor(user => user.FirstName)
            .NotNull()
            .NotEmpty()
            .MinimumLength(1)
            .MaximumLength(100)
            .WithMessage("Поле FirstName должно быть от 1 до 100");
        
        RuleFor(user => user.LastName)
            .NotNull()
            .NotEmpty()
            .MinimumLength(1)
            .MaximumLength(100)
            .WithMessage("Поле LastName должно быть от 1 до 100");

        RuleFor(user => user.Age)
            .NotNull()
            .NotEmpty()
            .GreaterThan(0)
            .LessThan(120);

        RuleFor(user => user.PhoneNumber)
            .MinimumLength(7)
            .MaximumLength(11);

        RuleFor(user => user.Role)
            .NotNull()
            .IsInEnum();
    }
}*/