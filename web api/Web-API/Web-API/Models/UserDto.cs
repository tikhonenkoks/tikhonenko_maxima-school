﻿/*using DataAccessLayer.Models;

namespace Web_API.Models;

/// <summary>
/// Класс User DataTransferObject 
/// </summary>
public class UserDto
{
    /// <summary>
    /// Идентификатор пользователя
    /// </summary>
    public int ID { get; set; }
    /// <summary>
    /// Логин
    /// </summary>
    public string Login { get; set; }
    /// <summary>
    /// Имя
    /// </summary>
    public string? FirstName { get; set; }
    /// <summary>
    /// Фамилия
    /// </summary>
    public string? LastName { get; set; }
    /// <summary>
    /// Возраст
    /// </summary>
    public int Age { get; set; }
   /// <summary>
   /// Номер телефона
   /// </summary>
    public string? PhoneNumber { get; set; }
    /// <summary>
    /// Уровень доступа, права пользователя
    /// </summary>
    public Role Role { get; set; }
}
*/