﻿/*using AutoMapper;
using DataAccessLayer.Models;
using Web_API.Models;

namespace Web_API.Mapper
{
    /// <summary>
    /// Автомаппер пользователя и модели в базе данных
    /// </summary>
    public class UserMapConfig : Profile
    {
        /// <summary>
        /// Правила маппинга пользователя и моделb в базе данных
        /// </summary>
        public UserMapConfig()
        {
            CreateMap<User, UserDto>()
                .ForMember(dto => dto.ID, expression => expression.MapFrom(user => user.ID))
                .ForMember(dto => dto.FirstName, expression => expression.MapFrom(user => user.FirstName))
                .ForMember(dto => dto.LastName, expression => expression.MapFrom(user => user.LastName))
                .ForMember(dto => dto.Age, expression => expression.MapFrom(user => user.Age))
                .ForMember(dto => dto.PhoneNumber, expression => expression.MapFrom(user => user.PhoneNumber))
                .ForMember(dto => dto.Role, expression => expression.MapFrom(user => user.Role));
            //.ForMember(dto => dto.Tickets, expression => expression.MapFrom(user => user.Tickets));

            CreateMap<UserDto, User>()
                .ForMember(user => user.ID, expression => expression.MapFrom(dto => dto.ID))
                .ForMember(user => user.FirstName, expression => expression.MapFrom(dto => dto.FirstName))
                .ForMember(user => user.LastName, expression => expression.MapFrom(dto => dto.LastName))
                .ForMember(user => user.Age, expression => expression.MapFrom(dto => dto.Age))
                .ForMember(user => user.PhoneNumber, expression => expression.MapFrom(dto => dto.PhoneNumber))
                .ForMember(user => user.Role, expression => expression.MapFrom(dto => dto.Role));
                //.ForMember(user => user.Tickets, expression => expression.MapFrom(dto => dto.Tickets));
        }
    }
}
*/