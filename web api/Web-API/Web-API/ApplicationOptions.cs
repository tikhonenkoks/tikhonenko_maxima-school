﻿namespace Web_API
{
    public class ApplicationOptions
    {
        public LoggingOptions Logging { get; set; }
        public string AllowedHosts { get; set; }
        public string DbConnection { get; set; }
        public string Secret { get; set; }
    }

    public class LoggingOptions
    {
        public LogLevel LogLevel { get; set; }
    }

    public class LogLevel
    {
        public string Default { get; set; }
        public string MicrosoftAspNetCore { get; set; }
    }
}
