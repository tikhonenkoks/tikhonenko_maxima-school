﻿using Web_API.Middlewares;

namespace Web_API.Extensions;

/// <summary>
/// Класс расширения для использования ExceptionsHandlerCustomMiddleware
/// </summary>
public static class ApplicationBuilderExtensions
{
    /// <summary>
    /// Метод расширения
    /// </summary>
    /// <param name="applicationBuilder"></param>
    public static void UseExceptionsHandlerCustomMiddleware(this IApplicationBuilder applicationBuilder)
    {
        applicationBuilder.UseMiddleware<ExceptionsHandlerCustomMiddleware>();
    }
}
