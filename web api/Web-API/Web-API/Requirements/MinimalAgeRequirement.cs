﻿using DataAccessLayer;
using Microsoft.AspNetCore.Authorization;

namespace Web_API.Requirements
{
    /// <summary>
    /// Требования к авторизации 
    /// </summary>
    public class MinimalAgeRequirement : IAuthorizationRequirement
    {
        public int Age { get; set; }

        public MinimalAgeRequirement(int age)
        {
            Age = age;
        }
    }


    public class MinimalAgeRequirementHandler : AuthorizationHandler<MinimalAgeRequirement>
    {
        private readonly DataContext _dataContext;

        public MinimalAgeRequirementHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, MinimalAgeRequirement requirement)
        {
            var claim = context.User.Claims.FirstOrDefault(claim => claim.Type == "userId");
            if(claim != null)
            {
                if (int.TryParse(claim.Value, out var id))
                {
                    var user = _dataContext.Users.FirstOrDefault(u => u.ID == id);
                    if (user != null)
                    {
                        if(user.Age >= requirement.Age)
                        {
                            context.Succeed(requirement);
                            return Task.CompletedTask;
                        }
                    }
                }
            }
            context.Fail();
            return Task.CompletedTask;
        }
    }
}
