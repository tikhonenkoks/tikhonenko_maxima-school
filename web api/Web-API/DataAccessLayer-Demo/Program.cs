﻿using DataAccessLayer;
using Microsoft.EntityFrameworkCore;

var optionsBuilder = new DbContextOptionsBuilder<DataContext>().UseNpgsql("Host=localhost;Port=5432;Database=Users;Username=postgres;Password=Shemas14!");
var dataContext = new DataContext(optionsBuilder.Options);

dataContext.Users.Add(new DataAccessLayer.Models.User()
{
    Age = 20,
    LastName = "fedorov",
    FirstName = "ivan",
    Role = DataAccessLayer.Models.RoleData.Admin,
    PhoneNumber = "+791565432448",
    Login = "administrator",
    Password = "Qwerty123"
});

dataContext.SaveChanges();
Console.ReadKey();