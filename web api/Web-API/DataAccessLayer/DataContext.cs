﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Configurations;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer
{
    public class DataContext: DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        // новый Dbset для класса наследника от Ticket 
        public DbSet<NewTicket> NewTickets { get; set; }


        public DataContext(DbContextOptions<DataContext> options): base(options)
        {
            Database.Migrate();// обновление БД при запуске приложения
        }

        // fluent api - описываются взаимосвязи и конфигурации моделей и таблиц
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserConfiguration).Assembly); // добавление конфигурации в modelBuilder
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(TicketConfiguration).Assembly); // добавление конфигурации в modelBuilder
        }
    }
}
