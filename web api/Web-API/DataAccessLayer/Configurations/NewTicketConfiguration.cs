﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    // fluent api - набор правил для NewTicket -вызывается в datacontext
    public class NewTicketConfiguration : IEntityTypeConfiguration<NewTicket>
    {
        public void Configure(EntityTypeBuilder<NewTicket> modelBuilder)
        {
            modelBuilder.ToTable("NewTickets"); // смена наименования таблицы
        }
    }
}

