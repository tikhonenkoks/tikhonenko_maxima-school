﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    // fluent api - набор правил для User -вызывается в datacontext
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> modelBuilder)
        {
            modelBuilder.HasKey(user => user.ID); // указание на первичный ключ
            modelBuilder.Property("Password").IsRequired();
        }
    }
}
