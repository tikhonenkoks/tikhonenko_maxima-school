﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    // fluent api - набор правил для Ticket -вызывается в datacontext
    public class TicketConfiguration : IEntityTypeConfiguration<Ticket>
    {
        public void Configure(EntityTypeBuilder<Ticket> modelBuilder)
        {
            modelBuilder.HasKey(ticket => ticket.ID); // указание на первичный ключ
            //modelBuilder.HasOne<User>(ticket => ticket.User)
            //    .WithMany(user => user.Tickets);// один ко многим. у одного пользователя мб много тикетов
            modelBuilder.ToTable("Tickets"); // смена наименования таблицы
            modelBuilder.Property("Genre").HasMaxLength(100); // у свойства Genre макс длина 100 
        }
    }
}

