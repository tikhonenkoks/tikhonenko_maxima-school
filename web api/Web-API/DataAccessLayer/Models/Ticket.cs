﻿using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    public class Ticket
    {
        public int ID { get; set; }

        public string? NameMovie { get; set; }

        public string? Genre { get; set; }

        public string? AgeRating { get; set; }

        public int OrderNum { get; set; }

        public DateTime SessionTime { get; set; }

        public double Cost { get; set; }

        public int HallNum { get; set; }

        public int Row { get; set; }

        public int Place { get; set; }

        //public User? User { get; set; }// навигационное свойство, связываем tickets с users
    }
}
