﻿namespace DataAccessLayer.Models
{
    public enum RoleData
    {
        Disable=1,
        Guest=2,
        User=3,
        Admin=4
    }
}
