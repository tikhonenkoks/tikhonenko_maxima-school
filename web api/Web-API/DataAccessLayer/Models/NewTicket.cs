﻿namespace DataAccessLayer.Models
{
    // создаем класс наследник от Ticket
    // прописываем новое свойство Description 
    public class NewTicket : Ticket
    {
        public string? Description { get; set; }
    }
}
