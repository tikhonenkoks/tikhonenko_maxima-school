﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    // класс, созданный для миграции
    // Когда вызовем миграцию, она найдет этот класс и с помощью него создаст экземпляр datacontext и подключится к базе данных
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>()
                .UseNpgsql("Host=localhost;Port=5432;Database=Users;Username=postgres;Password=Shemas14!");
            return new DataContext(optionsBuilder.Options);

        }
    }
}
