﻿namespace HW_17;

public static class Tasks
{
    public static void Task1(List<int> a, Random random, int k)
        {
            for (int i = 0; i < 10; i++)
            {
                a.Add(random.Next(-100, 100));
            }

            Console.WriteLine("Массив A: ");
            foreach (var item in a)
            {
                Console.WriteLine(item);
            }

            var result = a
                .Where(x => x > 0)
                .Where(x => x % 2 == 0)
                .Except(a.Skip(k))
                .Distinct()
                .Reverse()
                .ToList();

            Console.WriteLine("\nРезультат: ");
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }
    
        public static void Task2(List<int> a, List<int> b, Random random, int k1, int k2)
        {
            for (int i = 0; i < 10; i++)
            {
                b.Add(random.Next(-100, 100));
            }

            Console.WriteLine("Массив A: ");
            foreach (var item in a)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Массив B: ");
            foreach (var item in b)
            {
                Console.WriteLine(item);
            }

            var result = a
                .Where(x => x > k1)
                .Union(b.Where(q => q < k2))
                .OrderBy(order => order);

            Console.WriteLine("\nРезультат: ");
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }


        }

    
        public static void Task3(this List<Abiturients> abiturients)
        {
            var result = abiturients
                .OrderBy(order => order.YearOfStart)
                .GroupBy(group => group.SchoolNumber)
                .ToList()
                .OrderBy(order => order.Key)
                .Select(s => new
                {
                    SchoolNumber = s.Key,
                    Count = s.Count(),
                    Lastname = s.Select(l => l.LastName).First()
                });

            foreach (var item in result)
            {
                Console.WriteLine($" Номер школы: {item.SchoolNumber} | Число абитуриентов: {item.Count} | Фамилия : {item.Lastname}");
            }
        }

}