﻿using HW_17;

Random random = new Random();

int k1 = random.Next(10, 50);
int k2 = random.Next(50, 100);

List<int> a = new List<int>();
List<int> b = new List<int>();

List<Abiturients> abiturients = new List<Abiturients>();
abiturients.Add(new Abiturients { LastName = "Fedorov", YearOfStart = 2008, SchoolNumber = 1 });
abiturients.Add(new Abiturients { LastName = "Petrov", YearOfStart = 2008, SchoolNumber = 2 });
abiturients.Add(new Abiturients { LastName = "Sidorov", YearOfStart = 2014, SchoolNumber = 2 });
abiturients.Add(new Abiturients { LastName = "Sidorov", YearOfStart = 2010, SchoolNumber = 1 });
abiturients.Add(new Abiturients { LastName = "Sidorov", YearOfStart = 2012, SchoolNumber = 3 });
abiturients.Add(new Abiturients { LastName = "Tikhonov", YearOfStart = 2012, SchoolNumber = 3 });
abiturients.Add(new Abiturients { LastName = "Tikhonov", YearOfStart = 2011, SchoolNumber = 3 });


Console.WriteLine("=====Задание 1=====");
Tasks.Task1(a, random, k1);

Console.WriteLine("\n=====Задание 2=====");
Tasks.Task2(a, b, random, k1, k2);

Console.WriteLine("\n=====Задание 3=====");
Tasks.Task3(abiturients);