﻿namespace HW_8;

public class VC2 : VacuumCleaner
{
    public VC2() : base()
    {
    }
    private string model = "LG";
    public override string Model_Property { get=> model; set=> model = value; }

    public override void StartCleaning()
    {
        Console.WriteLine($"Уборку начал {Model_Property}");
    }
}