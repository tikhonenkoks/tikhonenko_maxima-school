﻿// See https://aka.ms/new-console-template for more information
using HW_8;

VacuumCleaner[] vc = new VacuumCleaner[3];
vc[0] = new VC1();
vc[1] = new VC2();
vc[2] = new VC3();

for (int i = 0; i < vc.Length; i++)
{
  vc[i].StartCleaning();
}

var vc1 = new VC3();
vc1.StartCleaning();