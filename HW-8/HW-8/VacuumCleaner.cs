﻿namespace HW_8;

public class VacuumCleaner
{
    protected string _model;
    private string room;
    public virtual string Model_Property 
    {
        get => _model;
        set => _model = value;
    }

    public virtual string Room
    {
        get=> room;
        set => room = value;
    }

    public VacuumCleaner()
    {
    }
    public virtual void StartCleaning()
    {
        Console.WriteLine("Уборка началась");
    }

}