﻿namespace HW_8;

public class VC1 : VacuumCleaner
{
    public VC1() : base()
    {
    }

    private string model = "Samsung";

    public override string Model_Property { get=> model; set=> model = value; }

    public override void StartCleaning()
    {
        base.StartCleaning();
    }
}