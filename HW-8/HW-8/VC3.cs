﻿namespace HW_8;
public class VC3 : VacuumCleaner
{
    public VC3() : base()
    {
    }
    private string model = "Xiaomi";
    public override string Model_Property { get=> model; set=> model = value; }

    private string _room = "Кухня";
    public override string Room { get=> _room; set=> _model=value; }

    public new void StartCleaning()
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine($"Уборку начал {model} в комнате: {Room}");
    }
}