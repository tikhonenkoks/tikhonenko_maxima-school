﻿namespace HW_11;

public class Rectangle : IFigure, INamable
{
    private int a;
    private int b;
    private string name;
    
    public int GetSquare()
    {
        return a*b;
    }

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    public Rectangle(int a, int b, string name)
    {
        this.a = a;
        this.b = b;
        this.name = name;
    }
}