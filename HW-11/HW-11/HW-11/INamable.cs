﻿namespace HW_11;

public interface INamable
{
    string Name { get; }
}