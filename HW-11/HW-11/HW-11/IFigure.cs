﻿namespace HW_11;

public interface IFigure
{
    int GetSquare();
}